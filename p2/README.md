> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Devin Hartley

*Main Readme:*

1. [Main README.md](../README.md "My Main README.md file")

### Project 2 Requirements:

*Sub-Heading:*

1. Answer project questions
2. Provide screenshot of MongoDB shell command
3. Provide screenshot of a required report and JSON code

#### README.md file should include the following items:

* Screenshot of MongoDB shell command
* Screenshot of required report and JSON code

#### Assignment Screenshots:

*Screenshot of MongoDB shell command*:

![shell command](img/shell.png)

*Screenshot of report and JSON*:

![JSON](img/json.png)