> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Devin Hartley

*Main Readme:*

1. [Main README.md](../README.md "My Main README.md file")

### Assignment 2 Requirements:

*Sub-Heading:*

1. Create customer and company tables using only SQL
2. Provide screenshots of SQL code
3. Provide screenshots of SQL result sets

#### README.md file should include the following items:

* Screenshot of A2 SQL Code
* Screenshot of A2 Result Set

#### Assignment Screenshots:

*Screenshot of A2 Code Part 1*:

![A2 Code Part 1](img/a2_company.png)

*Screenshot of A2 Code Part 2*:

![A2 Code Part 2](img/a2_customer.png)

*Screenshot of Result Set*:

![A2 Result Set](img/a2_result_set.png)