> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781

## Devin Hartley

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Create ERD according to business rules
    - Answer questions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create customer and company tables using only SQL
    - Provide screenshots of SQL code
    - Provide screenshots of SQL result sets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create customer, commodity, and order tables using only SQL
    - Provide screenshots of SQL code
    - Provide screenshots of populated tables within Oracle

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create tables using SQL code
    - Provide screenshots of P1 result sets
    - Provide screenshots of P1 ERD

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create A4 tables using only SQL
    - Provide screenshots of A4 ERD
    - Answer A4 assignment questions

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create A5 tables using only SQL
    - Provide screenshots of A5 ERD
    - Answer A5 assignment questions

6. [P2 README.md](p2/README.md "My P2 README.md file")
    - Answer P2 questions
    - Provide screenshot of MongoDB shell command
    - Provide screenshot of required report and JSON code