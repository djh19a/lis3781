> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Devin Hartley

*Main Readme:*

1. [Main README.md](../README.md "My Main README.md file")

### Project 1 Requirements:

*Sub-Heading:*

1. Create tables using SQL code
2. Provide screenshots of P1 result sets
3. Provide screenshots of P1 ERD

#### README.md file should include the following items:

* Screenshot of P1 SQL Code
* Screenshot of P1 ERD

#### Assignment Screenshots:

*Screenshot of P1 ERD*:

![P1 ERD](img/p1_erd.png)

*Screenshot of P1 Person Table Result Set*:

![P1 Person Table](img/p1_person.png)