> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Devin Hartley

*Main Readme:*

1. [Main README.md](../README.md "My Main README.md file")

### Assignment 3 Requirements:

*Sub-Heading:*

1. Create customer, commodity, and order tables using only SQL
2. Provide screenshots of SQL code
3. Provide screenshots of populated tables within Oracle

#### README.md file should include the following items:

* Screenshot of A3 SQL Code
* Screenshot of A3 Result Sets

#### Assignment Screenshots:

*Screenshot of A3 Code Part 1*:

![A3 Code Part 1](img/create_statements.png)

*Screenshot of A3 Code Part 2*:

![A3 Code Part 2](img/insert_statements.png)

*Screenshot of Customer Table*:

![Customer Table](img/customer.png)

*Screenshot of Commodity Table*:

![Commodity Table](img/commodity.png)

*Screenshot of Order Table*:

![Order Table](img/order.png)