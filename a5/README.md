> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Devin Hartley

*Main Readme:*

1. [Main README.md](../README.md "My Main README.md file")

### Assignment 5 Requirements:

*Sub-Heading:*

1. Create A5 tables using only SQL
2. Provide screenshots of A5 ERD
3. Answer A5 assignment questions

#### README.md file should include the following items:

* Screenshot of A5 ERD

#### Assignment Screenshots:

*Screenshot of A5 ERD*:

![A5 ERD](img/a5.png)