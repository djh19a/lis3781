> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Devin Hartley

### Assignment 1 Requirements:

*Sub-Heading:*

1. Install AMPPS
2. Make and forward engineer an ERD according to the given business rules
3. Answer questions

#### README.md file should include the following items:

* Screenshot of AMPPS installation
* Screenshot of A1 ERD

> #### Git commands w/short descriptions:

1. git init - create an empty git repository or reinitialize an existing one
2. git status - show the working tree status
3. git add - add file contents to the index
4. git commit - record changes to the repository
5. git push - update remote refs along with associated objects
6. git pull - fetch from and integrate with another repository or local branch
7. git clone - clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of ERD*:

![A1 ERD](img/ERD.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/djh19a/bitbucketstationlocations/ "Bitbucket Station Locations")
